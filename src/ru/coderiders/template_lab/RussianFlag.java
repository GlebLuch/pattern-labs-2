package ru.coderiders.template_lab;

public class RussianFlag extends Flag{

    @Override
    public String setCountry() {
        return "России";
    }

    @Override
    public String setFirstColor() {
        return "Белая";
    }

    @Override
    public String setSecondColor() {
        return "Синяя";
    }

    @Override
    public String setThirdColor() {
        return "Красная";
    }
}
