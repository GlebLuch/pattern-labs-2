package ru.coderiders.template_lab;

public abstract class Flag {

    void showFlag() {
        System.out.println("Флаг " + setCountry());
        System.out.println(setFirstColor() + " Полоса");
        System.out.println(setSecondColor() + " Полоса");
        System.out.println(setThirdColor() + " Полоса");
    }

    public abstract String setCountry();

    public abstract String setFirstColor();

    public abstract String setSecondColor();

    public abstract String setThirdColor();

}
