package ru.coderiders.template_lab;

public class GermanFlag extends Flag {

    @Override
    public String setCountry() {
        return "Германии";
    }

    @Override
    public String setFirstColor() {
        return "Черная";
    }

    @Override
    public String setSecondColor() {
        return "Красная";
    }

    @Override
    public String setThirdColor() {
        return "Желтая";
    }
}
