package ru.coderiders.template_lab;

public class Main {
    public static void main(String[] args) {

        Flag russianFlag = new RussianFlag();
        Flag germanFlag = new GermanFlag();

        russianFlag.showFlag();
        System.out.println();
        germanFlag.showFlag();

    }
}
