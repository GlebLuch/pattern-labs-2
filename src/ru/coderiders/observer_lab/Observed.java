package ru.coderiders.observer_lab;

import java.util.List;

public interface Observed {
    void add(Observer observer);
    void remove(Observer observer);
    void removeAll();
    void notifyObservers();
}
