package ru.coderiders.observer_lab;

import java.util.List;

public interface Observer {
    void handleEvent(List<String> events);
}
