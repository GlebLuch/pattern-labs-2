package ru.coderiders.observer_lab;

import java.util.List;

public class Subscriber implements Observer {
    String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void handleEvent(List<String> events) {
        System.out.println("Dear " + name + "\nWe have new event:\n" + events
                + "\n================================\n");
    }
}
