package ru.coderiders.observer_lab;

public class Main {
    public static void main(String[] args) {
        PublishingHouse publishingHouse = new PublishingHouse();

        publishingHouse.addEvent("Скидка на журнал");
        publishingHouse.addEvent("Подарок при покупке");

        Observer subscriber1 = new Subscriber("Глеб");
        Observer subscriber2 = new Subscriber("Виталя");

        publishingHouse.add(subscriber1);
        publishingHouse.add(subscriber2);

        publishingHouse.addEvent("Розыгрыш билетов");

        publishingHouse.removeEvent("Скидка на журнал");

        publishingHouse.remove(subscriber1);

        publishingHouse.removeEvent("Подарок при покупке");

        publishingHouse.removeAll();

        publishingHouse.add(subscriber1);

        publishingHouse.addEvent("Подарок при покупке");
    }
}
