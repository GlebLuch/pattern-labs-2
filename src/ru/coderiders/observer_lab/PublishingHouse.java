package ru.coderiders.observer_lab;

import java.util.ArrayList;
import java.util.List;

public class PublishingHouse implements Observed {

    List<String> events = new ArrayList<>();
    List<Observer> subscribers = new ArrayList<>();

    public void addEvent (String event) {
        this.events.add(event);
        notifyObservers();
    }

    public void removeEvent(String event) {
        this.events.remove(event);
        notifyObservers();
    }

    @Override
    public void add(Observer observer) {
        this.subscribers.add(observer);
    }

    @Override
    public void remove(Observer observer) {
        this.subscribers.remove(observer);
    }

    @Override
    public void removeAll() {
        this.subscribers.removeAll(subscribers);
    }

    @Override
    public void notifyObservers() {
        for(Observer observer : subscribers) {
            observer.handleEvent(this.events);
        }
    }
}
