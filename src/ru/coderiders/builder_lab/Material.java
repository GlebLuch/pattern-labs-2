package ru.coderiders.builder_lab;

public enum Material {
    CRYSTAL,
    GLASS,
    PLASTIC;
}
