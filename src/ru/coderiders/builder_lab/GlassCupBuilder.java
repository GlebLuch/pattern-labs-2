package ru.coderiders.builder_lab;

public class GlassCupBuilder extends CupBuilder{
    @Override
    void buildManufacturer() {
        cup.setManufacturer("OOO Кукшка");
    }

    @Override
    void buildMaterial() {
        cup.setMaterial(Material.GLASS);
    }

    @Override
    void buildPrice() {
        cup.setPrice(200);
    }
}
