package ru.coderiders.builder_lab;

public class Cup {
    private String manufacturer;
    private Material material;
    private Integer price;

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Cup{" +
                "manufacturer='" + manufacturer + '\'' +
                ", material=" + material +
                ", price=" + price +
                '}';
    }
}
