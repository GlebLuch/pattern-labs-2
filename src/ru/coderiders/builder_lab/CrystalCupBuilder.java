package ru.coderiders.builder_lab;

public class CrystalCupBuilder extends CupBuilder{
    @Override
    void buildManufacturer() {
        cup.setManufacturer("Pepsi");
    }

    @Override
    void buildMaterial() {
        cup.setMaterial(Material.CRYSTAL);
    }

    @Override
    void buildPrice() {
        cup.setPrice(300);
    }
}
