package ru.coderiders.builder_lab;

public class PlasticCupBuilder extends CupBuilder {
    @Override
    void buildManufacturer() {
        cup.setManufacturer("Матрешка");
    }

    @Override
    void buildMaterial() {
        cup.setMaterial(Material.PLASTIC);
    }

    @Override
    void buildPrice() {
        cup.setPrice(50);
    }
}
