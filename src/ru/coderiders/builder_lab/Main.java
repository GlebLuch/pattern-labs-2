package ru.coderiders.builder_lab;

public class Main {
    public static void main(String[] args) {
        Director director = new Director();

        director.setBuilder(new GlassCupBuilder());

        Cup cup = director.buildCup();
        System.out.println(cup);
    }
}
