package ru.coderiders.builder_lab;

public abstract class CupBuilder {

    Cup cup;

    public void createCup() {
        cup = new Cup();
    }

    abstract void buildManufacturer();
    abstract void buildMaterial();
    abstract void buildPrice();

    Cup getCup() {
        return cup;
    }
}
