package ru.coderiders.builder_lab;

public class Director {

    CupBuilder cupBuilder;

    public void setBuilder(CupBuilder cupBuilder) {
        this.cupBuilder = cupBuilder;
    }

    Cup buildCup() {
        cupBuilder.createCup();
        cupBuilder.buildManufacturer();
        cupBuilder.buildMaterial();
        cupBuilder.buildPrice();

        return cupBuilder.getCup();
    }
}
