package ru.coderiders.state_lab;

public class Caterpillar implements InsectState{
    @Override
    public void grow(Insect insect) {
        System.out.println("Гусеница становится коконом");
        insect.insectState = new Cocoon();
    }

    @Override
    public void doIt() {
        System.out.println("Гусеница ползет");
    }

}
