package ru.coderiders.state_lab;

public class Butterfly implements InsectState{

    @Override
    public void grow(Insect insect) {
        System.out.println("А некуда ей дальше расти, только смерть");
    }

    @Override
    public void doIt() {
        System.out.println("Бабочка Летит");
    }
}
