package ru.coderiders.state_lab;

public interface InsectState {
    void grow(Insect insect);
    void doIt();
}
