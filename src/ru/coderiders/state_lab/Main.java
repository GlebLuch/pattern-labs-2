package ru.coderiders.state_lab;

public class Main {
    public static void main(String[] args) {
        Insect insect = new Insect(new Caterpillar());

        insect.doIt();
        insect.grow();
        insect.doIt();
        insect.grow();
        insect.doIt();
        insect.grow();
    }
}
