package ru.coderiders.state_lab;

public class Cocoon implements InsectState {
    @Override
    public void grow(Insect insect) {
        System.out.println("Кокон становится бабочкой");
        insect.insectState = new Butterfly();
    }

    @Override
    public void doIt() {
        System.out.println("Кокон спит");
    }
}
