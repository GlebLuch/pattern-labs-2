package ru.coderiders.state_lab;

public class Insect {

    public InsectState insectState;

    public InsectState getInsectState() {
        return insectState;
    }

    public void setInsectState(InsectState insectState) {
        this.insectState = insectState;
    }

    public Insect(InsectState insectState) {
        this.insectState = insectState;
    }

    public void grow() {
        insectState.grow(this);
    }

    public void doIt() {
        insectState.doIt();
    }
}
