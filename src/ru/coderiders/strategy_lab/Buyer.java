package ru.coderiders.strategy_lab;

public class Buyer {

    Payment payment;

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void pay() {
       payment.pay();
    }
}
