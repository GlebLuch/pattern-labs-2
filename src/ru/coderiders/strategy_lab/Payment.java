package ru.coderiders.strategy_lab;

public interface Payment {
    void pay();
}
