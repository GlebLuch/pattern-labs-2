package ru.coderiders.strategy_lab;

public class CashCoinsPayment implements Payment {
    @Override
    public void pay() {
        System.out.println("Оплатить монетами");
    }
}
