package ru.coderiders.strategy_lab;

public class TransferPayment implements Payment {
    @Override
    public void pay() {
        System.out.println("Оплата переводом");
    }
}
