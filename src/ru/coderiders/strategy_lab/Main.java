package ru.coderiders.strategy_lab;

public class Main {
    public static void main(String[] args) {
        Buyer buyer = new Buyer();

        buyer.setPayment(new CashPaperPayment());
        buyer.pay();

        buyer.setPayment(new CashCoinsPayment());
        buyer.pay();

        buyer.setPayment(new CardPayment());
        buyer.pay();

        buyer.setPayment(new TransferPayment());
        buyer.pay();
    }
}
