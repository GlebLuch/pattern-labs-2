package ru.coderiders.strategy_lab;

public class CardPayment implements Payment {
    @Override
    public void pay() {
        System.out.println("Оплатить картой");
    }
}
