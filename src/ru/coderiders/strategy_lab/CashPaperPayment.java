package ru.coderiders.strategy_lab;

public class CashPaperPayment implements Payment {
    @Override
    public void pay() {
        System.out.println("Оплатить бумажной наличкой");
    }
}
