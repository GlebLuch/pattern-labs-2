package ru.coderiders.factory_lab.coffee;

public class Espresso implements Coffee{
    @Override
    public void grind() {
        System.out.println("Зерна перемолоты");
    }

    @Override
    public void cook() {
        System.out.println("Эспрессо готовится");
    }

    @Override
    public void pour() {
        System.out.println("Эспрессо налит в чашку");
    }

    @Override
    public void give() {
        System.out.println("Эспрессо отдан");
    }
}
