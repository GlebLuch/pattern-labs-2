package ru.coderiders.factory_lab.coffee;

public class Cappuccino implements Coffee {
    @Override
    public void grind() {
        System.out.println("Зерна перемолоты");
    }

    @Override
    public void cook() {
        System.out.println("Капучино готовится");
    }

    @Override
    public void pour() {
        System.out.println("Капучино налит в чашку");
    }

    @Override
    public void give() {
        System.out.println("Капучино отдан");
    }
}
