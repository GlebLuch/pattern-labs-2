package ru.coderiders.factory_lab.coffee;

public interface Coffee {
    void grind();
    void cook();
    void pour();
    void give();
}
