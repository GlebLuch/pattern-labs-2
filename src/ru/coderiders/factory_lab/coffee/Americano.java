package ru.coderiders.factory_lab.coffee;

public class Americano implements Coffee{
    @Override
    public void grind() {
        System.out.println("Зерна перемолоты");
    }

    @Override
    public void cook() {
        System.out.println("Американо готовится");
    }

    @Override
    public void pour() {
        System.out.println("Американо налит в чашку");
    }

    @Override
    public void give() {
        System.out.println("Американо отдан");
    }
}
