package ru.coderiders.factory_lab.coffee;

public class Latte implements Coffee{
    @Override
    public void grind() {
        System.out.println("Зерна перемолоты");
    }

    @Override
    public void cook() {
        System.out.println("Латте готовится");
    }

    @Override
    public void pour() {
        System.out.println("Латте налит в чашку");
    }

    @Override
    public void give() {
        System.out.println("Латте отдан");
    }
}
