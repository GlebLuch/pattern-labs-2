package ru.coderiders.factory_lab;

import ru.coderiders.factory_lab.factory.CoffeeShop;
import ru.coderiders.factory_lab.factory.CoffeeType;
import ru.coderiders.factory_lab.factory.Factory;

public class Main {

    public static void main(String[] args) {
        Factory factory = new Factory();
        CoffeeShop coffeeShop = new CoffeeShop(factory);
        coffeeShop.orderCoffee(CoffeeType.LATTE);
    }
}
