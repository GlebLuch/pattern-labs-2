package ru.coderiders.factory_lab.factory;

public enum CoffeeType {
    LATTE,
    CAPPUCCINO,
    AMERICANO,
    ESPRESSO
}
