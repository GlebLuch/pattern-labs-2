package ru.coderiders.factory_lab.factory;

import ru.coderiders.factory_lab.coffee.Coffee;

public class CoffeeShop {

    private final Factory coffeeFactory;

    public CoffeeShop(Factory coffeeFactory) {
        this.coffeeFactory = coffeeFactory;
    }

    public void orderCoffee(CoffeeType type) {
        Coffee coffee = coffeeFactory.createCoffee(type);
        coffee.grind();
        coffee.cook();
        coffee.pour();
        coffee.give();

        System.out.println("Вот ваш кофе! Спасибо, приходите еще!");
    }
}
