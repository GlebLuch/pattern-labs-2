package ru.coderiders.factory_lab.factory;

import ru.coderiders.factory_lab.coffee.*;

public class Factory {

    public Coffee createCoffee (CoffeeType type) {
        Coffee coffee = null;

        switch (type) {
            case AMERICANO:
                coffee = new Americano();
                break;
            case ESPRESSO:
                coffee = new Espresso();
                break;
            case CAPPUCCINO:
                coffee = new Cappuccino();
                break;
            case LATTE:
                coffee = new Latte();
                break;
        }

        return coffee;
    }
}
