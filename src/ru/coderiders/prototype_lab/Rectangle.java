package ru.coderiders.prototype_lab;

public class Rectangle implements Figure {

    int width;
    int height;

    public Rectangle(int width) {
        this.width = width;
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public Figure clone() {
        return new Rectangle(this.width, this.height);
    }

    @Override
    public void getInfo() {
        System.out.printf("Прямоугольник длинной %d и ширинной %d%n", height, width);
    }
}
