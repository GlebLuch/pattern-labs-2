package ru.coderiders.prototype_lab;

public class Main {
    public static void main(String[] args) {

        Figure figure = new Rectangle(30, 40);
        Figure clonedFigure = figure.clone();

        Figure square = new Square(10);
        square.getInfo();
        figure.getInfo();
        clonedFigure.getInfo();
    }
}
