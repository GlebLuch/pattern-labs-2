package ru.coderiders.prototype_lab;

public class Square extends Rectangle {

    public Square(int width) {
        super(width);
    }

    public Figure clone() {
        return new Square(this.width);
    }

    public void getInfo() {
        System.out.printf("Квадрат с стороной %d%n", width);
    }
}
