package ru.coderiders.prototype_lab;

public interface Figure {

    Figure clone();
    void getInfo();
}
