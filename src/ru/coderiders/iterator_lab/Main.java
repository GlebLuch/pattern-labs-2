package ru.coderiders.iterator_lab;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> issues = List.of("Забрать поссылку с почты",
                "Сходить в спортзал",
                "Встретится с друзьями"
        );

        User user = new User("Пятница", issues);

        Iterator iterator = user.getIterator();
        System.out.println("День недели: " + user.getDay());
        System.out.println("Задачи: ");

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + "\n");
        }
    }
}
