package ru.coderiders.iterator_lab;

import java.util.List;

public class User implements Collector {

    private String day;
    private List<String> issues;

    public User(String name, List<String> issues) {
        this.day = name;
        this.issues = issues;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setIssues(List<String> issues) {
        this.issues = issues;
    }

    public String getDay() {
        return day;
    }

    public List<String> getIssues() {
        return issues;
    }

    @Override
    public Iterator getIterator() {
        return new IssuesIterator();
    }

    public class IssuesIterator implements Iterator {

        int index;

        @Override
        public boolean hasNext() {
            return index < issues.size();
        }

        @Override
        public String next() {
            return issues.get(index++);
        }
    }
}
