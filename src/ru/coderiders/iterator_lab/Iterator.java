package ru.coderiders.iterator_lab;

import java.util.Objects;

public interface Iterator {
    boolean hasNext();
    String next();
}
