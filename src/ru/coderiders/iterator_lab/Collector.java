package ru.coderiders.iterator_lab;

public interface Collector {
    Iterator getIterator();
}
