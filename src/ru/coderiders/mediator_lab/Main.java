package ru.coderiders.mediator_lab;

public class Main {
    public static void main(String[] args) {
        SimpleChat chat = new SimpleChat();

        User dispatcher = new Dispatcher(chat, "Dispatcher");
        User pilot1 = new Pilot(chat, "Pilot1");
        User pilot2 = new Pilot(chat, "Pilot2");

        chat.setDispatcher(dispatcher);
        chat.addUserToChat(pilot1);
        chat.addUserToChat(pilot2);

        pilot1.sendMessage("Надвигаюсь на посадку");
        dispatcher.sendMessage("Я диспетчер");
    }
}
