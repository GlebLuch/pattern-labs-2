package ru.coderiders.mediator_lab;

public interface Chat {
    void sendMessage(String message, User pilot);
}
