package ru.coderiders.mediator_lab;

import java.util.ArrayList;
import java.util.List;

public class SimpleChat implements Chat {

    User dispatcher;
    List<User> pilots = new ArrayList<>();

    public void setDispatcher(User user) {
        this.dispatcher = user;
    }

    public void addUserToChat(User user) {
        this.pilots.add(user);
    }

    @Override
    public void sendMessage(String message, User pilot) {
        for (User p : pilots) {
            if (p != pilot) {
                p.getMessage(message);
            }
        }
        dispatcher.getMessage(message);
    }
}
