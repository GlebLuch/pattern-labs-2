package ru.coderiders.mediator_lab;

public class Pilot implements User {

    Chat chat;
    String name;

    public Pilot(Chat chat, String name) {
        this.chat = chat;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void sendMessage(String message) {
        chat.sendMessage(message, this);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(this.name + " получил сообщение: " + message + ".");
    }
}
